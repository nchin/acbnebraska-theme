'use strict';

(function() {
    let form_items = document.querySelectorAll('.form-item');
    for (let i = 0; i < form_items.length; ++i) {
        if (form_items[i].classList.contains('form-item--error')) {

            if (form_items[i].nodeName === 'FIELDSET') {
                //Not a good way to add an aria-described by to a fieldset. Leave it as is.
                continue;
            }

            let error_message = form_items[i].querySelector('.form-item--error-message');
            let input = form_items[i].querySelector('[aria-invalid]');
            let input_id = input.getAttribute('id');

            //link the error message with aria-described by
            if (!error_message) {
                //There isn't an error message for some reason
                continue;
            }


            if (!input_id) {
                //No input id for some odd reason
                continue;
            }

            let message_id = input_id+'_error_msg';
            error_message.setAttribute('id', message_id);
            input.setAttribute('aria-describedby', message_id);
        }
    }

    //Try to make it more obvious that there was an error
    let message = document.querySelector('.messages.messages--error');
    if (message) {
        message.setAttribute('tabindex','-1');
        message.focus();
    }
})();